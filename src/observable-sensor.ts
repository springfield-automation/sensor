import { Sensor } from './sensor';

export type ValueObserver<T> = (error: Error | null | undefined, value: T) => void;

export interface ObservableSensor<T> extends Sensor<T> {
  addObserver(observer: ValueObserver<T>): void;
  removeObserver(observer: ValueObserver<T>): void;
}
