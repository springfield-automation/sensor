export interface Sensor<T> {
  getCurrentValue(): Promise<T>
}