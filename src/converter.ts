export interface Converter<T> {
  convert(value: T): T
}